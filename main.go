package main

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
)

func main() {
	http.HandleFunc("/", index)
	http.HandleFunc("/send", bdSend)
	//处理静态文件
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))
	err := http.ListenAndServe(":80", nil)
	if err != nil {
		log.Fatal(err)
	}
}

//首页
func index(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		t, err := template.ParseFiles("views/index.html")
		if err != nil {
			log.Fatal(err)
		}
		t.Execute(w, nil)
	}
}

//推送
func bdSend(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		request(w, r)
	}
}

//模拟http客户端post发送请求
func request(w http.ResponseWriter, r *http.Request) {
	//先解析form
	err := r.ParseForm()	
	if err != nil {
		log.Fatal(err)
	}
	//请求地址
	var apiURL string
	switch r.Form["oper"][0] {
	case "post":
		apiURL = "http://data.zz.baidu.com/urls"
	case "update":
		apiURL = "http://data.zz.baidu.com/update"
	case "del":
		apiURL = "http://data.zz.baidu.com/del"
	}

	//初始化参数
	param := url.Values{}

	//配置请求参数,方法内部已处理urlencode问题,中文参数可以直接传参
	param.Set("site", r.Form["site"][0])
	param.Set("token", r.Form["token"][0])

	var URL *url.URL
	URL, err = url.Parse(apiURL)
	if err != nil {
		fmt.Printf("解析url错误:\r\n%v", err)
	}
	//如果参数中有中文参数,这个方法会进行URLEncode
	URL.RawQuery = param.Encode()
	client := &http.Client{}
	req, _ := http.NewRequest("POST", URL.String(), strings.NewReader(r.Form["sendURL"][0]))

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	//req.Header.Set("User-Agent", "curl/7.12.1")
	//req.Header.Set("Host", "data.zz.baidu.com")

	//发送请求
	resp, err := client.Do(req)
	//注意关闭resp.Body
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf("请求失败,错误信息:\r\n%v", err)
	} else {
		//var returns map[string]interface{}
		//json.Unmarshal(data, &returns)
		//data, err = json.Marshal(returns)
		//将返回的json数据返给客户端浏览器
		fmt.Fprintln(w, string(data))
	}
}
